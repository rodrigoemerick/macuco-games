Shader "MyShaders/Waterfall"
{
    Properties
    {
        _NoiseTex("Noise Texture", 2D) = "white" {}
        _DisplacementGuide("Displacement Guide", 2D) = "white" {}
        _DisplacementAmount("Displcament Amount", Float) = 0
        _ColorBottomDark("Color Bottom Dark", Color) = (1.0, 1.0, 1.0, 1.0)
        _ColorTopDark("Color Top Dark", Color) = (1.0, 1.0, 1.0, 1.0)
        _ColorBottomLight("Color Bottom Light", Color) = (1.0, 1.0, 1.0, 1.0)
        _ColorTopLight("Color Top Light", Color) = (1.0, 1.0, 1.0, 1.0)
        _BottomFoamThreshold("Bottom Foam Threshold", Range(0.0, 1.0)) = 0.1
        _SpeedY("Speed Y", Float) = 0

    }

    Subshader
    {
        Tags {"RenderType" = "Opaque"}
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;
            sampler2D _DisplacementGuide;
            float4 _DisplacementGuide_ST;
            float4 _ColorBottomDark;
            float4 _ColorTopDark;
            float4 _ColorBottomLight;
            float4 _ColorTopLight;
            float _DisplacementAmount;
            float _BottomFoamThreshold;
            float _SpeedY;

            struct appdata
            {
                float4 pos : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 noiseUV : TEXCOORD1;
                float2 displUV : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.pos);
                o.noiseUV = TRANSFORM_TEX(v.uv, _NoiseTex);
                o.displUV = TRANSFORM_TEX(v.uv, _DisplacementGuide);
                o.uv = v.uv + fixed2 (0, _SpeedY * _Time.y * 1000)
                UNITY_TRANSFER_FOG(o, o.pos);

                return o;
            }

            float4 frag (v2f i) : SV_TARGET
            {
                float noise = tex2D(_NoiseTex, float2(i.noiseUV.x, i.noiseUV.y + _Time.y / 5)).y;
                noise = round(noise * 5.0) / 5.0;

                float4 col = lerp(lerp(_ColorBottomDark, _ColorTopDark, i.uv.x), lerp(_ColorBottomLight, _ColorTopLight, i.uv.x), noise);
                col = lerp(float4(1.0, 1.0, 1.0, 1.0), col, step(_BottomFoamThreshold, i.uv.y));
                UNITY_APPLY_FOG(i.fogCoord, col); 

                return col;
            }
            ENDCG
        }
    }
    Fallback "VertexLit"
}