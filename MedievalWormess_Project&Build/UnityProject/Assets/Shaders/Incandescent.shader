Shader "MyShaders/Incandescent"
{
    Properties
    {
        _IncandescentTex ("Incandescent", 2D) = "white" {}
        _NoiseTex ("Noise", 2D) = "black" {}
        _Flow1 ("Flow 1", Vector) = (1.0, 0.0, 0.0, 0.0)
        _Flow2 ("Flow 2", Vector) = (-1.0, -1.0, 0.0, 0.0)
        _Pulse ("Pulse", Float) = 1.0
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _IncandescentTex;
            sampler2D _NoiseTex;
            fixed4 _IncandescentTex_ST;
            fixed4 _NoiseTex_ST;
            fixed4 _Flow1;
            fixed4 _Flow2;
            fixed _Pulse;

            struct appdata
            {
                fixed4 pos : POSITION;
                fixed2 uv : TEXCOORD0;
            };

            struct v2f
            {
                fixed4 pos : SV_POSITION;
                fixed2 incandescent_uv : TEXCOORD0;
                fixed2 noise_uv : TEXCOORD1;
            };

            v2f vert (appdata inputFromApp)
            {
                v2f v;
                v.pos = UnityObjectToClipPos(inputFromApp.pos);
                v.incandescent_uv = TRANSFORM_TEX(inputFromApp.uv, _IncandescentTex);
                v.incandescent_uv += _Flow1.xy * _Time.x;
                v.noise_uv = TRANSFORM_TEX(inputFromApp.uv, _NoiseTex);
                v.noise_uv += _Flow2.xy * _Time.x;
                return v;
            }

            fixed4 frag (v2f i) : SV_TARGET
            {
                fixed4 noise = tex2D(_NoiseTex, i.noise_uv);
                fixed2 pertube = noise.xy * 0.5 - 0.5;

                fixed4 incandescentcol = tex2D(_IncandescentTex, i.incandescent_uv + pertube);
                fixed pulse = tex2D(_NoiseTex, i.noise_uv + pertube).a;

                fixed4 temper = incandescentcol * pulse * _Pulse + (incandescentcol * incandescentcol - 0.1);
                
                if (temper.r > 1.0)
                {
                    temper.bg += clamp(temper.r - 2.0, 0.0, 5.0);
                }

                if (temper.g > 1.0)
                {
                    temper.rb += temper.g - 1.0;
                }

                if (temper.b > 1.0)
                {
                    temper.rg += temper.b - 1.0;
                }

                incandescentcol = temper;
                incandescentcol.a = 1.0;
                return incandescentcol;
            }
            ENDCG

        }
    }
}