﻿
Shader "ToonShader"
{
    Properties
    {
        Kd("Kd(diffuse)", Color) = (1.0, 1.0, 1.0, 1.0)
        Ks("Ks(specullar)", Color) = (1.0, 1.0, 1.0, 1.0)
        MyImage("Source Image", 2D) = "Black"{}

        specularShiness("Brilho Especular", Range(0.0, 1024.0)) = 0.5
        diffuseShiness("Brilho Difuso", Range(0.0, 1.0)) = 0.5
        strokeSize("Stroke", Range(0.0, 1.0)) = 0.3
    }
    SubShader
    {
        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "Lighting.cginc"

            fixed4 Kd;
            fixed4 Ks;
            fixed specularShiness;
            fixed diffuseShiness;
            fixed strokeSize;
            sampler2D MyImage;


            struct appdata
            {
                float4 pos : POSITION;
                float3 normal : NORMAL0;
                float2 uv : TEXCOORD1;
            };

            struct v2f
            {
                float4 pos : SV_POSITION;
                float3 normal : TEXCOORD0;
                float3 view : TEXCOORD2;
                float2 uv : TEXCOORD1;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.pos);
                o.normal = normalize(mul(v.normal, unity_ObjectToWorld));
                o.view = normalize(_WorldSpaceCameraPos - o.normal);
                o.uv = v.uv;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 lightDirectionNormallized = normalize(_WorldSpaceLightPos0);
                fixed3 reflection = normalize(2 * dot(i.normal, lightDirectionNormallized) * i.normal - lightDirectionNormallized);

                fixed specular = pow(max(dot(reflection, i.view),0.0), specularShiness);
                fixed4 diffuse = max(dot(i.normal, lightDirectionNormallized), 0.0);
                fixed edgeLight = max(dot(i.normal, i.view), 0);

                fixed4 diffuseComponent = Kd * _LightColor0 * lerp(0.2, 1.0, step(diffuseShiness, diffuse));
                fixed4 specularComponent = Ks * _LightColor0 * lerp(0.1, 1.0, step(0.2, specular));
                fixed4 Stroke = lerp(0.0, 1.0, step(strokeSize, edgeLight));

                fixed ImageToPut = tex2D (MyImage, i.uv);

                fixed4 lighting = ImageToPut * Stroke * (saturate(diffuseComponent) 
                                           + saturate(specularComponent));

                return lighting;
            }
            ENDCG
        }
    }
}

/*
 Diego Jesus Almeida, 31806311


 -------------- Referências bibliográficas ----------
The Cg Tutorial, capítulo 9.2


 -------------- Explicação do código ----------------

O toon shader é a modificação do shader phong (junção de difuse e specular) com a diferença de que usa variáveis
discretas invés de usar as contínuas.

Foi passado nos properties as cores do brilho difuso e specular. Também foram passados os controladores dos mesmos
No appdata foi passado as normais e a posição e no v2f, além destes dois, é passada uma view

Na função vert, foram passados os componentes da cena para o v2f
Na função frag, são feitos todos os cálculos, dentre eles os principais são: 
    - É passada a direção da luz e a reflexão (levando em consideração o negativo da posição da luz para que não exista reflexão especular na parte da sombra)
    - É calculada a luz difusa
    - É calculada a luz especular
    - É calculada a projeção do contorno
Porém todos estão com gradiente, então foram usados o lerp combinado com o step, dessa forma as variáveis contínuas viram discretas
Na hora de retornar a cor, foi multiplicado o stroke pela soma de todos os outros componentes.


*/