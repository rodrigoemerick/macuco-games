﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinCare : MonoBehaviour
{
    [SerializeField] GeneralInfoSO m_GGISO;
    [SerializeField] int m_PlayerID;
    [SerializeField] GameObject m_PlayerBeard;
    public int PlayerID
    {
        set 
        {
            m_PlayerID = value;
            ChangeBeardColor();
        }
    }

    void Update()
    {
        ChangeBeardColor();
    }

    void ChangeBeardColor()
    {
        Color temp = Color.black;
        switch (m_PlayerID)
        {
            case 0:
                temp = m_GGISO.playersColors[0];
                break;
            case 1:
                temp = m_GGISO.playersColors[1];
                break;
            case 2:
                temp = m_GGISO.playersColors[2];
                break;
            case 3:
                temp = m_GGISO.playersColors[3];
                break;
        }
        m_PlayerBeard.GetComponent<SkinnedMeshRenderer>().material.color = temp;
    }
}

