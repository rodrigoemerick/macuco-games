﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerCommander : MonoBehaviour
{
    [SerializeField] string m_MyIndex;
    public string MyIndex { get => m_MyIndex; }

    bool m_IsConnected;
    public bool IsConnected
    {
        get => m_IsConnected;
        set => m_IsConnected = value;
    }

    bool m_CanGoNext = true;

    private Player m_RewiredPlayer;

    [SerializeField]
    private float m_DelayTime;

    bool m_IsMenu = true;
    public bool IsMenu
    {
        set => m_IsMenu = value;
    }

    public delegate void ConfirmCommandHandler(string index);
    public event ConfirmCommandHandler ConfirmCommand;

    public delegate void DenyCommandHandler(string index);
    public event DenyCommandHandler DenyCommand;

    public delegate void MoveCommandHandler(string side, string index);
    public event MoveCommandHandler MoveCommand;

    public delegate void MoveHorizontalHandler(float moviment);
    public event MoveHorizontalHandler MoveHorizontal;

    public delegate void MoveVerticalHandler(float moviment);
    public event MoveVerticalHandler MoveVertical;

    public delegate void SmashHandler();
    public event SmashHandler Smash;


    private void Awake()
    {
        m_RewiredPlayer = ReInput.players.GetPlayer("Player" + m_MyIndex);
        DontDestroyOnLoad(this.gameObject);
    }
    void Update()
    {
        if (m_IsMenu)
        {
            if (m_RewiredPlayer.GetButtonDown("Confirm"))
            {
                OnConfirmCommand();
            }

            if (m_RewiredPlayer.GetButtonDown("Deny"))
            {
                OnDenyCommand();
            }

            if (m_CanGoNext)
            {
                if (m_RewiredPlayer.GetAxis("UINavLeft") < -0.5f || m_RewiredPlayer.GetButtonDown("UINavLeft"))
                {
                    OnMoveCommand("left");
                    m_CanGoNext = false;
                    StartCoroutine(LetGoNext());
                }

                if (m_RewiredPlayer.GetAxis("UINavRight") > 0.5f || m_RewiredPlayer.GetButtonDown("UINavRight"))
                {
                    OnMoveCommand("right");
                    m_CanGoNext = false;
                    StartCoroutine(LetGoNext());
                }

                if (m_RewiredPlayer.GetAxis("UINavUp") > 0.5f || m_RewiredPlayer.GetButtonDown("UINavUp"))
                {
                    OnMoveCommand("up");
                    m_CanGoNext = false;
                    StartCoroutine(LetGoNext());
                }

                if (m_RewiredPlayer.GetAxis("UINavDown") < -0.5f || m_RewiredPlayer.GetButtonDown("UINavDown"))
                {
                    OnMoveCommand("down");
                    m_CanGoNext = false;
                    StartCoroutine(LetGoNext());
                }
            }
        }
        else
        {
            OnMoveHorizontal(m_RewiredPlayer.GetAxis("Movement Horizontal"));
            OnMoveVertical(m_RewiredPlayer.GetAxis("Movement Vertical"));

            if (m_RewiredPlayer.GetButtonDown("Smash"))
            {
                Smash();
            }
        }
    }

    public void ChangeControlMap(bool menuValue, bool gameplayValue)
    {
        m_RewiredPlayer.controllers.maps.SetMapsEnabled(gameplayValue, "Gameplay");
        m_RewiredPlayer.controllers.maps.SetMapsEnabled(menuValue, "MenuUI");
        if (menuValue)
        {
            m_IsMenu = true;
        }
        else
        {
            m_IsMenu = false;
        }
    }

    void OnConfirmCommand()
    {
        ConfirmCommand?.Invoke((int.Parse(m_MyIndex) + 1).ToString());
    }

    void OnDenyCommand()
    {
        DenyCommand?.Invoke((int.Parse(m_MyIndex) + 1).ToString());
    }

    void OnMoveCommand(string side)
    {
        MoveCommand?.Invoke(side, (int.Parse(m_MyIndex) + 1).ToString());
    }

    void OnMoveHorizontal(float value)
    {
        MoveHorizontal?.Invoke(value);
    }

    void OnMoveVertical(float value)
    {
        MoveVertical?.Invoke(value);
    }

    void OnSmash()
    {
        Smash?.Invoke();
    }

    IEnumerator LetGoNext()
    {
        yield return new WaitForSeconds(m_DelayTime);
        m_CanGoNext = true;
    }
}
