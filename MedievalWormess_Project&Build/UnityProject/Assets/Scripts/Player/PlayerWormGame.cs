﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWormGame : PlayerClass
{
    [SerializeField]
    private float m_ThrustIfWormSpawnBelow;
    [SerializeField]
    private float m_StunTime;
    [SerializeField]
    private AudioSource m_StunAudio;

    private bool m_IsStunned;

    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();

        m_Animator.SetBool("Stun", m_IsStunned);
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
    }

    protected override void OnTriggerEnter(Collider other)
    {
        if (this.enabled)
        {
            if (other.gameObject.CompareTag("RespawnFall"))
            {
                StartCoroutine(RespawnAfterFall());
            }

            if (other.gameObject.CompareTag("WormTop"))
            {
                m_Rigidbody.AddForce(transform.forward * m_ThrustIfWormSpawnBelow, ForceMode.Impulse);
            }

            if (other.gameObject.CompareTag("Hammer"))
            {
                m_IsStunned = true;
                m_Speed = 0.0f;
                StartCoroutine(StunReset());
                if (!m_StunAudio.isPlaying)
                    m_StunAudio.Play();
            }
        }
    }

    protected override void OnCollisionEnter(Collision other)
    {

    }

    IEnumerator StunReset()
    {
        yield return new WaitForSeconds(m_StunTime);
        m_IsStunned = false;
        m_Speed = m_TempSpeed;
    }
}