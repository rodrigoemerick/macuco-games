﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameStateMachine<T>
{
    protected T parent;

    private Dictionary<string, GameState<T>> stateDictionary;

    protected GameState<T> currentState;
    public GameState<T> CurrentState
    {
        get { return currentState; }
    }

    public GameStateMachine(T Parent)
    {
        parent = Parent;

        stateDictionary = new Dictionary<string, GameState<T>>();

        currentState = null;
    }

    public void Update()
    {
        if (currentState != null)
        {
            currentState.Update();
        }
    }

    public bool IsInState(string stateName)
    {
        if (currentState == null) return false;

        return currentState == stateDictionary[stateName];
    }

    public void ChangeState(string stateName)
    {
        if (!stateDictionary.ContainsKey(stateName))
        {
            Debug.LogError($"ChangeState({stateName}): state not found.");
            return;
        }

        if (stateDictionary[stateName] == currentState)
        {
            //Debug.LogError($"ChangeState({stateName}): new state is already the current state.");
            return;
        }

        if (currentState != null)
            currentState.Exit();

        currentState = stateDictionary[stateName];
        currentState.Enter();
    }

    public void ClearCurrentState()
    {
        if (currentState != null)
            currentState.Exit();

        currentState = null;
    }

    public bool AddState(string stateName, GameState<T> state, bool overwriteIfExists = true)
    {
        if (state == null)
        {
            Debug.LogError($"AddState({stateName}, {state}, {overwriteIfExists}): state is null");
            return false;
        }

        if (overwriteIfExists)
        {
            stateDictionary[stateName] = state;
        }
        else
        {
            try
            {
                stateDictionary.Add(stateName, state);
            }
            catch (Exception e)
            {
                Debug.LogError($"AddState({stateName}, {state}, {overwriteIfExists}): error trying to add new state");
                return false;
            }
        }

        return true;
    }

    public bool RemoveState(string stateName, bool forceIfInUse = false)
    {
        if (stateDictionary.ContainsKey(stateName))
        {
            if (currentState == stateDictionary[stateName])
            {
                if (forceIfInUse)
                {
                    Debug.LogWarning($"RemoveState({stateName}): removing current state");

                    currentState.Exit();
                    currentState = null;

                    return stateDictionary.Remove(stateName);
                }
                else
                {
                    Debug.LogError($"RemoveState({stateName}): trying to remove current state");
                    return false;
                }
            }
        }

        Debug.LogError($"RemoveState({stateName}): state not found");
        return false;
    }
}