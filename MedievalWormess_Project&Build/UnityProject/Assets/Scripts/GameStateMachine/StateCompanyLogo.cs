﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StateCompanyLogo : GameState<GameManager> 
{
    public StateCompanyLogo(GameManager parent) : base(parent)
    {
    }

    public override void Enter()
    {
        parent.StartCoroutine(ChangeState());
        parent.StartCoroutine(PlayBreakEgg());
        parent.StartCoroutine(PlayMacucoAudio());
    }

    public override void Exit()
    {

    }

    public override void Update()
    {
       
    }

    public override IEnumerator ChangeState()
    {
        yield return new WaitForSeconds(2.8f);
        parent.ChangeState("transMenu");
    }
    
    IEnumerator PlayBreakEgg()
    {
        yield return new WaitForSeconds(0.1f);
        parent.PlayLogoAudios(1);
    }

    IEnumerator PlayMacucoAudio()
    {
        yield return new WaitForSeconds(1.2f);
        parent.PlayLogoAudios(2);
    }
}
