﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatePause : GameState<GameManager>
{
    public StatePause(GameManager parent) : base(parent)
    {
    }

    public override IEnumerator ChangeState()
    {
        throw new System.NotImplementedException();
    }

    public override void Enter()
    {
        
    }

    public override void Exit()
    {
       
    }

    public override void Update()
    {

    }
}
