﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateBackToMenu : GameState<GameManager>
{
    public StateBackToMenu(GameManager parent) : base(parent)
    {
    }

    public override IEnumerator ChangeState()
    {
        return null;
    }

    public override void Enter()
    {
        
    }

    public override void Exit()
    {
        parent.MenuController = GameObject.Find("MenuController");
        parent.MenuController.GetComponent<MenuController>().SetUpIsFinish += parent.OnSetupIsFinish;
        parent.MenuController.GetComponent<MenuController>().CloseGameSelected += parent.OnCloseGameSelected;

        for (int i = 0; i < parent.PlayerCommanders.Length; i++)
        {
            parent.PlayerCommanders[i].GetComponent<PlayerCommander>().ConfirmCommand += parent.MenuController.GetComponent<MenuController>().OnConfirmCommand;
            parent.PlayerCommanders[i].GetComponent<PlayerCommander>().DenyCommand += parent.MenuController.GetComponent<MenuController>().OnDenyCommand;
            parent.PlayerCommanders[i].GetComponent<PlayerCommander>().MoveCommand += parent.MenuController.GetComponent<MenuController>().OnMoveCommand;
        }

        for(int i = 0; i < parent.PlayerCommanders.Length; i++)
        {
            parent.PlayerCommanders[i].ChangeControlMap(true ,false);
        }

        parent.MenuController.GetComponent<MenuController>().ChangeConnectedPlayers(parent.PlayersConnect);
        parent.MenuController.GetComponent<MenuController>().ChangeCurrentMenu("player");

    }

    public override void Update()
    {
        if (parent.SceneIsLoad)
        {
            parent.ChangeState("menu");
        }
    }
}
