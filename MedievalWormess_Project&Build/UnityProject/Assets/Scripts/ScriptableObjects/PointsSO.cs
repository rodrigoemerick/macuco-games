﻿using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "Scriptable Objects/Points Info", order = 4)]
public class PointsSO : ScriptableObject
{
    public bool[] playerIsInGame;
    public bool[] playerIsDead;
    public int[] playerPoints;
    public int playerWinner;
}
