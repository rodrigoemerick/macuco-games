﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptableObject", menuName = "Scriptable Objects/Worms Info", order = 3)]
public class WormsInfoSO : ScriptableObject
{
    [Header("Geral")]

    public bool miniGameStart;
    public GameObject holePrefab;
    public GameObject wormPrefab;
    public List<GameObject> womsList;

    [Header("WormsInfo")]

    public float easyWormVelocity;
    public float mediumWormVelocity;
    public float hardWormVelocity;
    public float extremeWormVelocity;
    public float upDistance;
    public float smooth;
    
    [Header("CycleInfo")]

    public bool[] whitePresence;
    public bool[] colorPresence;
    public bool[] easyPresence;
    public bool[] mediumPresence;
    public bool[] hardPresence;
    public bool[] extremePresence;
    public float[] cycleTime;
    public int[] cycleMin;
    public int[] cycleMax;
}