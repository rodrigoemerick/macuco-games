﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    GameStateMachine<GameManager> m_FSM;
    [SerializeField] GeneralInfoSO m_GGISO;
    
    [SerializeField] PlayerCommander[] m_PlayerCommanders;
    public PlayerCommander[] PlayerCommanders { get => m_PlayerCommanders; }

    [SerializeField] GameObject m_InputManager;

    [Header("CompanyLogo Info")]
    [SerializeField] GameObject m_CompanyLogo;
    public GameObject CompanyLogo
    {
        get { return m_CompanyLogo; }
        set { m_CompanyLogo = value; }
    }

    [SerializeField] private AudioSource m_BreakEggAudio;
    [SerializeField] private AudioSource m_MacucoAudio;

  
    GameObject m_MenuController;

    string m_NumOfPlayers;
    public string NumOfPlayers { get => m_NumOfPlayers; }
   
    int[] m_PlayersConnect;
    public int[] PlayersConnect { get => m_PlayersConnect; }


    public GameObject MenuController
    {
        get { return m_MenuController; }
        set { m_MenuController = value; }
    }

    bool m_SceneIsLoad;
    public bool SceneIsLoad
    {
        get { return m_SceneIsLoad; }
        set { m_SceneIsLoad = value; }
    }

    bool m_CanGoToMiniGames = false;
    public bool CanGoToMiniGames
    {
        get => m_CanGoToMiniGames;
    }
    bool m_GameIsFinish;
    public bool GameIsFinish
    {
        get => m_GameIsFinish;
    }

    void Awake()
    {
        m_FSM = new GameStateMachine<GameManager>(this);
        m_FSM.AddState("logo", new StateCompanyLogo(this));
        m_FSM.AddState("transMenu", new StateTaransitionToMenu(this));
        m_FSM.AddState("menu", new StateMenu(this));
        m_FSM.AddState("pause", new StatePause(this));
        m_FSM.AddState("mini", new StateMiniGame(this, m_GGISO));
        m_FSM.AddState("tmg", new StateBackToMenu(this));
        m_FSM.ChangeState("logo");

        m_PlayersConnect = new int[4];

        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(m_InputManager);

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Update()
    {
        m_FSM.Update();
    }

    public void ChangeState(string state)
    {
        m_FSM.ChangeState(state);
    }

    public void PlayLogoAudios(int whichAudio)
    {
        switch (whichAudio)
        {
            case 1:
                m_BreakEggAudio.Play();
                break;
            case 2:
                m_MacucoAudio.Play();
                break;
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Menu")
        {
            m_SceneIsLoad = true;
        }
    }

    public void OnSetupIsFinish()
    {
        m_CanGoToMiniGames = true;
        m_GameIsFinish = false;
    }

    public void OnGameIsFinish()
    {
        m_GameIsFinish = true;
        m_CanGoToMiniGames = false;
        m_SceneIsLoad = false;
    }

    public void OnCloseGameSelected()
    {
        GetComponent<QuitGame>().Quit();
    }
}