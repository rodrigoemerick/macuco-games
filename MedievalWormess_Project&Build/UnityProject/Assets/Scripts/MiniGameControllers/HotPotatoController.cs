﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HotPotatoController : MiniGameController
{
    [SerializeField] private GameObject m_WormPotato;

    PlayerPotatoGame m_CurrentWithPotato;

    int m_PlayersAlive;

    private void Start()
    {
        m_PSO.playerWinner = -1;
    }

    void Update()
    {
        if (m_CanStart)
        {
            for (int i = 0; i < m_Players.Length; ++i)
            {
                if (m_Players[i].GetComponent<PlayerPotatoGame>().IsWithPotato)
                {
                    m_CurrentWithPotato = m_Players[i].GetComponent<PlayerPotatoGame>();
                    m_WormPotato.transform.position = m_CurrentWithPotato.PotatoPosition;
                }
            }
        }
    }

    private void SelectPlayerWithWorm()
    {
        int player = UnityEngine.Random.Range(0, 4);
        bool temp = false;
        while (!temp)
        {
            if (m_Players[player].activeSelf && !m_Players[player].GetComponent<PlayerPotatoGame>().HasExploded)
            {
                temp = true;
            }
            else 
            {
                player = UnityEngine.Random.Range(0, 4);
            } 
        }

        m_Players[player].GetComponent<PlayerPotatoGame>().IsWithPotato = true;
    }

    void CheckPlayersAlive()
    {
        m_PlayersAlive = 0;
        for (int i = 0; i <= m_Players.Length; i++)
        {
            if(m_Players[i].activeSelf && !m_Players[i].GetComponent<PlayerPotatoGame>().HasExploded)
            {
                m_PlayersAlive++;
            }
        }
    }

    public override void StartMiniGame()
    {
        m_WormPotato.SetActive(true);
        SelectPlayerWithWorm();
        base.StartMiniGame();
    }

    public override void OnMiniGameIsFinish()
    {
        if (m_PlayersAlive > 2)
        {
            m_CurrentWithPotato.HasExploded = true;
            m_CurrentWithPotato.IsWithPotato = false;
            m_PSO.playerIsDead[m_CurrentWithPotato.MyIndex] = true;
            CheckPlayersAlive();
            StartMiniGame();
        }
        else
        {
            m_CurrentWithPotato.HasExploded = true;
            m_PSO.playerIsDead[m_CurrentWithPotato.MyIndex] = true;
            m_PSO.playerWinner = m_CurrentWithPotato.MyIndex;
            m_CanStart = false;
            m_WormPotato.SetActive(false);
            base.OnMiniGameIsFinish();
        }
    }
}