﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MapDecoder))]
public class WormsController : MiniGameController
{
    [SerializeField] WormsInfoSO m_WISO;
    [SerializeField] GeneralInfoSO m_GGISO;

    MapDecoder m_MapDecoder;

    int m_CurrentCycle = 0;

    public override void StartMiniGame()
    {
        m_MapDecoder = GetComponent<MapDecoder>();
        m_MapDecoder.SetMapInfo();
        base.StartMiniGame();
        StartCoroutine(ChangeCycle());
    }

    private void Update()
    {
        if (m_CanStart)
        {
            MaintainWorms();
        }

        if(m_CurrentCycle >= 4)
        {
            m_CurrentCycle = 0;
        }
    }

    public override void OnMiniGameIsFinish()
    {
        m_CanStart = false;
        DeactiveAllWorms();
        base.OnMiniGameIsFinish();
    }

    void MaintainWorms()
    {
        if (CheckNumOfActiveWorms() < m_WISO.cycleMin[m_CurrentCycle])
        {
            DeterminateWormsFeatures();
        }
    }

    private void DeterminateWormsFeatures()
    {
        if (CheckMaxWorms())
        {
            Color color = DeterminateColor();
            float time = DeterminateType();
            int worm = DeterminateWorm();

            m_WISO.womsList[worm].GetComponent<Worms>().Appear(color, time);
        }
    }

    private Color DeterminateColor()
    {
       
        int valor = 0;
        Color c = Color.white;
        bool canGo = false;

        while (!canGo)
        {
            valor = Random.Range(0, 5);
            if(valor == 4) { canGo = m_WISO.whitePresence[m_CurrentCycle]; }
            else { canGo = m_WISO.colorPresence[m_CurrentCycle]; }
        }

        return m_GGISO.playersColors[valor]; 

    }
   
    private float DeterminateType()
    {
        int valor = 0;
        float f = 0;
        bool canGo = false;

        while (!canGo)
        {
            valor = Random.Range(0, 4);
            switch (valor)
            {
                case 0:
                    if (m_WISO.easyPresence[m_CurrentCycle])
                    {
                        canGo = true;
                        f = m_WISO.easyWormVelocity;
                    }
                    break;
                case 1:
                    if (m_WISO.mediumPresence[m_CurrentCycle])
                    {
                        canGo = true;
                        f = m_WISO.mediumWormVelocity;
                    }
                    break;
                case 2:
                    if (m_WISO.hardPresence[m_CurrentCycle])
                    {
                        canGo = true;
                        f = m_WISO.hardWormVelocity;
                    }
                    break;
                case 3:
                    if (m_WISO.extremePresence[m_CurrentCycle])
                    {
                        canGo = true;
                        f = m_WISO.extremeWormVelocity;
                    }
                    break;
              
            }
        }

        return f;
    }

    private int DeterminateWorm()
    {
        int worm = 0;

        bool canGo = true;

        while (canGo)
        {
            worm = Random.Range(0, m_WISO.womsList.Count);
            canGo = m_WISO.womsList[worm].GetComponent<Worms>().IsActive;
        }

        return worm;
    }

    int CheckNumOfActiveWorms()
    {
        int num = 0;

        for (int i = 0; i < m_WISO.womsList.Count; i++)
        {
            if (m_WISO.womsList[i].GetComponent<Worms>().IsActive)
            {
                num++;
            }
        }
        return num;
    }

    bool CheckMaxWorms()
    {
        if (CheckNumOfActiveWorms() == m_WISO.cycleMax[m_CurrentCycle])
        {
            return false;
        }
        return true;
    }

    IEnumerator ChangeCycle() 
    {
        yield return new WaitForSeconds(m_WISO.cycleTime[m_CurrentCycle]);
        if (m_CurrentCycle < 3)
        {
            m_CurrentCycle++;
            StartCoroutine(ChangeCycle());
        }

    }

    void DeactiveAllWorms()
    {
        for (int i = 0; i < m_WISO.womsList.Count; i++)
        {
            if (m_WISO.womsList[i].GetComponent<Worms>().IsActive)
            {
                m_WISO.womsList[i].GetComponent<Worms>().Disappear();
            }
        }
    }
}