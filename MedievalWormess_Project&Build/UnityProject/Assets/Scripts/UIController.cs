﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] GeneralInfoSO m_GGISO;
    [SerializeField] PointsSO m_PSO;

    [SerializeField] GameObject[] m_PlayersGeralUIGO;
    [SerializeField] GameObject[] m_PlayerDethGO;
    [SerializeField] GameObject[] m_PointsGO;
    [SerializeField] GameObject m_LeaderboardGO;
    [SerializeField] GameObject[] m_LeaderboardBars;
    [SerializeField] Text[] m_PointsLeaderboard;
    [SerializeField] Image[] m_ImageLeaderboard;

    bool m_GameIsRunnig;

    private void Update()
    {
        if (m_GameIsRunnig)
        {
            for (int i = 0; i < m_PSO.playerIsInGame.Length; i++)
            {
                if (m_PSO.playerIsInGame[i])
                {
                    if (m_GGISO.selectedMiniGame == "worms")
                    {
                        m_PointsGO[i].GetComponent<Text>().text = m_PSO.playerPoints[i].ToString();
                    }
                    else if (m_PSO.playerIsDead[i] && !m_PlayerDethGO[i].activeSelf && i != m_PSO.playerWinner)
                    {
                        m_PlayerDethGO[i].SetActive(true);
                    }
                    else
                    {
                        m_PointsGO[i].GetComponent<Text>().text = $"Player {i + 1}";
                    }
                }
            }
        }
    }

    public void ActiveUI()
    {
        for (int i = 0; i < m_GGISO.playersConnect.Length; i++)
        {
            m_PSO.playerIsInGame[i] = false;

            if (m_GGISO.playersConnect[i] == 1)
            {
                m_PSO.playerIsInGame[i] = true;
                m_PlayersGeralUIGO[i].SetActive(true);
                if (m_GGISO.selectedMiniGame == "worms") 
                {
                    m_PointsGO[i].SetActive(true);
                } 
            }

            m_PSO.playerPoints[i] = 0;
            m_PSO.playerIsDead[i] = false;
        }

        m_GameIsRunnig = true;
    }

    void DeactivateUI()
    {
        for (int i = 0; i < m_GGISO.playersConnect.Length; i++)
        {
            m_PlayersGeralUIGO[i].SetActive(false);
            m_PointsGO[i].SetActive(false);
        }

        m_GameIsRunnig = false;
    }

    public void ActiveLaderboard()
    {
        DeactivateUI();
        m_LeaderboardGO.SetActive(true);
        
        for (int i = 0; i < m_GGISO.playersConnect.Length; i++)
        {
            if (m_PSO.playerIsInGame[i])
            {
                m_LeaderboardBars[i].SetActive(true);
                
                if (m_GGISO.selectedMiniGame == "worms")
                {
                    m_PointsLeaderboard[i].gameObject.SetActive(true);
                    m_PointsLeaderboard[i].text = m_PSO.playerPoints[i].ToString();
                }
                else if (m_GGISO.selectedMiniGame == "smashery")
                {
                    if (m_PSO.playerIsDead[i])
                    {
                        m_ImageLeaderboard[i].gameObject.SetActive(true);
                        m_PointsLeaderboard[i].text = $"Player {i + 1}";
                    }
                    else
                    {
                        m_ImageLeaderboard[i].gameObject.SetActive(false);
                        m_PointsLeaderboard[i].text = $"Player {i + 1}";
                    }
                }
                else if (m_PSO.playerIsDead[i])
                {
                    m_ImageLeaderboard[i].gameObject.SetActive(false);
                    m_PointsLeaderboard[i].text = $"Player {i + 1}";
                }
                else
                {
                    m_ImageLeaderboard[i].gameObject.SetActive(true);
                    m_PointsLeaderboard[i].text = $"Player {i + 1}";
                }
            }
        }
    }
}