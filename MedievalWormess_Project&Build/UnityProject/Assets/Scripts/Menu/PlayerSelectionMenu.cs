﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerSelectionMenu : BaseMenu
{
    [SerializeField] SkinSelector[] playerSelector;
    Dictionary<string, SkinSelector> playerSelectorDict;

    [SerializeField] GameObject m_MenuImage;

    bool m_CanGoBack;

    public delegate void PlayerSetupIsFinishHendler(int numberOfPlayers, int[] playersConnected);
    public event PlayerSetupIsFinishHendler PlayerSetUpIsFinish;

    int[] m_PlayersConnected = {0, 0, 0, 0 };

    private void Awake()
    {
        playerSelectorDict = new Dictionary<string, SkinSelector>();
        SetSkinSelectorDictionary();
        NavegationButtonsDict = new Dictionary<string, NavegationButtons>();
        SetNavegationButtonsDict();
    }

    private void Update()
    {
        CheckNumberOfPlayers();
        CheckAllReadyPlayers();
        PreviousMenu = "openning";
    }

    public override void Confirm(string playerID)
    {
        if (!playerSelectorDict[playerID].PlayerConected)
        {
            playerSelectorDict[playerID].ConnectPLayer(true);
        }
        else if(playerSelectorDict[playerID].PlayerConected && !playerSelectorDict[playerID].PlayerReady)
        {
            playerSelectorDict[playerID].PlayerIsReady(true);
        }
    }

    public override void Back(string playerID)
    {
        if (!m_CanGoBack)
        {
            if (playerSelectorDict[playerID].PlayerReady)
            {
                playerSelectorDict[playerID].PlayerIsReady(false);
            }
            else if (playerSelectorDict[playerID].PlayerConected)
            {
                playerSelectorDict[playerID].ConnectPLayer(false);
            }

        }
        else if(playerSelectorDict[playerID].PlayerConected)
        {
            base.Back(playerID);
        }
    }

    public override void MenuMovimentation(string movimentSide, string playerID)
    {
       
    }

    public override void EnterInMenu()
    {
        foreach (string key in playerSelectorDict.Keys)
        {
            playerSelectorDict[key].gameObject.SetActive(true);
        }
        playerSelectorDict["1"].ConnectPLayer(true);
        m_MenuImage.SetActive(true);
    }

    public override void ExitMenu()
    {
        foreach (string key in playerSelectorDict.Keys)
        {
            playerSelectorDict[key].PlayerIsReady(false);
            playerSelectorDict[key].gameObject.SetActive(false);
        }
        m_MenuImage.SetActive(false);
    }

    void SetSkinSelectorDictionary()
    {
        for (int i = 0; i < playerSelector.Length; i++)
        {
            playerSelectorDict.Add(playerSelector[i].PlayerReference, playerSelector[i]);
        }
    }

    void CheckNumberOfPlayers()
    {
        int count = 0;
        foreach (string key in playerSelectorDict.Keys)
        {
            if (playerSelectorDict[key].PlayerConected)
            {
                m_PlayersConnected[int.Parse(key) - 1] = 1;
                count++;
            }
            else
            {
                m_PlayersConnected[int.Parse(key) - 1] = 0;
            }
        }

        if(count > 1 || CheckIfIsReady())
        {
            m_CanGoBack = false;
        }
        else
        {
            m_CanGoBack = true;
        }
    }

    bool CheckIfIsReady()
    {
        foreach (string key in playerSelectorDict.Keys)
        {
            if (playerSelectorDict[key].PlayerReady)
            {
                return true;
            }
        }
        return false;
    }

    void CheckAllReadyPlayers()
    {
        int playersConfirmed = 0;
        int playersReady = 0;

        foreach (string key in playerSelectorDict.Keys)
        {
            if (playerSelectorDict[key].PlayerConected)
            {
                playersConfirmed++;
            }

            if (playerSelectorDict[key].PlayerReady)
            {
                playersReady++;
            }
        }


        if(playersConfirmed == playersReady)
        {
             OnPlayerSetupIsFinish(playersReady, m_PlayersConnected);
        }
    }

    public void ChangeConnectedPlayers(int[] playersConnect)
    {
        m_PlayersConnected = playersConnect;
        for (int i = 0; i < 4; i++)
        {
            if (m_PlayersConnected[i] == 1)
            {
                playerSelectorDict[(i + 1).ToString()].ConnectPLayer(true);
            }
            else
            {
                playerSelectorDict[(i + 1).ToString()].ConnectPLayer(false);
            }
        }
    }

    void OnPlayerSetupIsFinish(int numberOfPlayers, int[] playersConnected)
    {
        if (PlayerSetUpIsFinish != null)
            PlayerSetUpIsFinish(numberOfPlayers, playersConnected);
    }
}