﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseMenu : MonoBehaviour
{
    [SerializeField] protected string m_MenuID;
    public string MenuID
    {
        get => m_MenuID;
    }

    protected string m_PreviousMenu = " ";
    public string PreviousMenu
    {
        get => m_PreviousMenu;
        set => m_PreviousMenu = value;
    }

    [SerializeField] protected NavegationButtons[] m_ThisMenuButtons;

    Dictionary<string, NavegationButtons> m_NavegationButtons;
    public Dictionary<string, NavegationButtons> NavegationButtonsDict
    {
        get => m_NavegationButtons;
        set => m_NavegationButtons = value;
    }

    [SerializeField] protected Button m_BackButton;

    string m_currentButton = "1";

    private void Awake()
    {
        m_NavegationButtons = new Dictionary<string, NavegationButtons>();
        SetNavegationButtonsDict();
    }

    public virtual void MenuMovimentation(string movimentSide, string playerID)
    {
        if (int.Parse(NavegationButtonsDict[m_currentButton].NeighborsButtons[movimentSide]) != 0)
        {
            NavegationButtonsDict[m_currentButton].OnChange();
            m_currentButton = NavegationButtonsDict[m_currentButton].NeighborsButtons[movimentSide];
            NavegationButtonsDict[m_currentButton].OnHighLite();
        }
    }

    public virtual void Confirm(string playerID) 
    {
        NavegationButtonsDict[m_currentButton].OnClick();
    }

    public virtual void Back(string playerID)
    {
        if (m_BackButton != null)
        {
            m_BackButton.onClick.Invoke();
        }
    }

    public virtual void EnterInMenu()
    {
        foreach (string key in NavegationButtonsDict.Keys)
        { 
            NavegationButtonsDict[key].gameObject.SetActive(true);
        }

        if (NavegationButtonsDict.Count > 0)
        {
            NavegationButtonsDict[m_currentButton].OnHighLite();
        }
    }

    public virtual void ExitMenu()
    {
        foreach (string key in NavegationButtonsDict.Keys)
        {
            NavegationButtonsDict[key].gameObject.SetActive(false);
        }
    }

    protected void SetNavegationButtonsDict()
    {
        for (int i = 0; i < m_ThisMenuButtons.Length; i++)
        {
            m_ThisMenuButtons[i].ButtonID = (i+1).ToString();
            m_NavegationButtons.Add(m_ThisMenuButtons[i].ButtonID, m_ThisMenuButtons[i]);
        }
    }


}
