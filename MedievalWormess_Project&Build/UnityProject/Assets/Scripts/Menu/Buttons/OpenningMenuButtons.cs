﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenningMenuButtons : NavegationButtons
{
    [SerializeField] Image m_BackGroundImg;
    [SerializeField] Text m_Text;

    public override void OnHighLite()
    {
        m_BackGroundImg.color = Color.green;
        m_Text.color = Color.white;
    }

    public override void OnChange()
    {
        m_BackGroundImg.color = Color.white;
        m_Text.color = Color.black;
    }
}
