﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionMenuButtons : NavegationButtons
{
    [SerializeField] SkinSelector m_MySkiinselctor;
    string m_MyPlayer;

    private void Awake()
    {
        m_MyPlayer = m_MySkiinselctor.PlayerReference;
    }

    public override void OnHighLite()
    {
        StartCoroutine(BackToDefaultColor());
        GetComponent<Image>().color = Color.cyan;
    }

    public override void OnClick()
    {
        if (m_MySkiinselctor.PlayerConected && !m_MySkiinselctor.PlayerReady)
        {
            OnHighLite();
            base.OnClick();
        }
    }

    private void Update()
    {
        if (!m_MySkiinselctor.PlayerConected)
        {
            GetComponent<Image>().color = Color.black;
        }
    }

    IEnumerator BackToDefaultColor()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<Image>().color = Color.white;
    }
}
